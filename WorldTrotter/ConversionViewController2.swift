//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by Connor Burk on 7/21/17.
//  Copyright © 2017 Connor Burk. All rights reserved.
//

import Foundation

import UIKit

class ConversionViewController: UIViewController {
    
    // label for calculated celsius value
    @IBOutlet var celsiusLabel: UILabel!
    
    // function: fahrenheitFieldEditingChanged
    // purpose: catch user input ( VALUE label )
    @IBAction func fahrenheitFieldEditingChanged(_ textField: UITextField) {
        
        if let text = textField.text, let value = Double(text) {        // check for user input and
                                                                        // validity as double
            fahrenheitValue = Measurement(value: value, unit: .fahrenheit) // set fahrenheit value to
                                                                           // measurement init as double
        } else {
            fahrenheitValue = nil
        }
        
    }
    
    
    // optional temperature measurement
    // fahrenheit variable
    var fahrenheitValue: Measurement<UnitTemperature>? {
        didSet {
            updateCelciusLabel() // PROPERTY OBSERVER: code called when a property's
                                 //                    value changes
                                 // purpose (in this program): auto update celsius value
                                 //                            upon user input
        }
    }
    
    // Function: celsiusValue
    // Purpose: Convert fahrenheit value to celsius value
    var celsiusValue: Measurement<UnitTemperature>? {
        if let fahrenheitValue = fahrenheitValue {
            return fahrenheitValue.converted(to: .celsius)
        } else {
            return nil
        }
    }
    
    // Method: updateCelsiusLabel
    // Purpose: update celsius label when 
    //          new fahrenheit value is received
    func updateCelciusLabel() {
        if let celsiusValue = celsiusValue {
            celsiusLabel.text = "\(celsiusValue.value)"
        } else {
            celsiusLabel.text = "???"
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}

