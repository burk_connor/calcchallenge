//
//  MapViewController.swift
//  WorldTrotterV4
//
//  Created by Connor Burk on 7/24/17.
//  Copyright © 2017 Connor Burk. All rights reserved.
//

import CoreLocation
import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    // function: currentLocation()
    // purpose: send user back to current location
    @IBAction func currentLocation(_ sender: UIButton) {
        locationManager.startUpdatingLocation()
    }

    
    // create instance of CLLocationManager
    var locationManager = CLLocationManager()
    
    // Method: mapTypeChanged
    // Purpose: switch map types
    //          standard, hybrid, satellite
    func mapTypeChanged(_ segControl: UISegmentedControl) {
        switch segControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .hybrid
        case 2:
            mapView.mapType = .satellite
        default:
            break
        }
    }
    
    // find current location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // check for last location found
        if let location = locations.last {
            
            // set zoom on location
            let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
            
            // find current location
            let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            
            // set up location region for map
            let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
            
            // set region on map
            mapView.setRegion(region, animated: true)
        }
        
        // show region on map
        self.mapView.showsUserLocation = true
        
        // Don't find current location until currentLocation() is called
        locationManager.stopUpdatingLocation()
        
        
    }
    
    override func loadView() {
        
        // create a map view
        mapView = MKMapView()
        
        // Set it as the view of this view controller
        view = mapView
        
        // segmented control
        // Change between map types
        let standardString = NSLocalizedString("Standard", comment: "Standard map view")
        let satelliteString = NSLocalizedString("Satellite", comment: "Satellite map view")
        let hybridString = NSLocalizedString("Hybrid", comment: "Hybrid map view")
        
        let segmentedControl = UISegmentedControl(items: [standardString, satelliteString, hybridString])
        
        // make background clear
        segmentedControl.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        
        segmentedControl.selectedSegmentIndex = 0
        
        // target-action pair associated with .valueChanged event
        segmentedControl.addTarget(self, action: #selector(MapViewController.mapTypeChanged(_:)), for: .valueChanged)
        
        // disable auto constraints
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        
        // add segmented control to interface
        view.addSubview(segmentedControl)
        
        // place segmented control below time
        // using layout guides
        let topConstraint = segmentedControl.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 8)
        
        // current location button
        
        
        // margins are better than constants on constraints
        // universal amongst devices
        let margins = view.layoutMarginsGuide
        
        let leadingConstraint = segmentedControl.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
        
        let trailingConstraint = segmentedControl.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
        
        // activate constraints
        topConstraint.isActive = true
        leadingConstraint.isActive = true
        trailingConstraint.isActive = true
        
        
        
    }
    
    
    // current location button
    let button = UIButton()
    
    // list of programmed constraints
    var buttonCons:[NSLayoutConstraint] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // drop pins
        
        let span2:MKCoordinateSpan = MKCoordinateSpanMake(0.1, 0.1)
        
        // location of the arch 
        // coordinats from google maps
        let theArch:CLLocationCoordinate2D = CLLocationCoordinate2DMake(38.624942, -90.184841)
        
        // set region (ARCH)
        let archRegion:MKCoordinateRegion = MKCoordinateRegionMake(theArch, span2)
        
        // set area on map
        mapView.setRegion(archRegion, animated: true)
        
        // customize pin
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = theArch
        annotation.title = "The Gateway Arch"
        annotation.subtitle = "My city"
        
        // add to map
        mapView.addAnnotation(annotation)
        
        // *****
        // pin 2
        // *****
        
        let span3:MKCoordinateSpan = MKCoordinateSpanMake(0.1, 0.1)
        
        // location of the arch
        let dragonGate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(37.790883, -122.405646)
        
        // set region (ARCH)
        let dragonGateRegion:MKCoordinateRegion = MKCoordinateRegionMake(theArch, span2)
        
        // place on map
        mapView.setRegion(dragonGateRegion, animated: true)
        
        // customize pin
        let annotation2 = MKPointAnnotation()
        
        annotation2.coordinate = dragonGate
        annotation2.title = "The Dragon's Gate"
        annotation2.subtitle = "Test"
        
        // add to map
        mapView.addAnnotation(annotation2)
        
        // function: createButton()
        // purpose: programmatically create button
        func createButton() {
            
            // get rid of auto constraints
            button.translatesAutoresizingMaskIntoConstraints = false
            
            button.setTitle("Current Location", for: .normal)
            button.setTitleColor(UIColor.black, for: .normal)
            button.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            
            // add button to view
            mapView.addSubview(button)
            
            // programmed constraints
            let buttonHeight = button.heightAnchor.constraint(equalToConstant: 100)
            let buttonWidth = button.widthAnchor.constraint(equalToConstant: 250)
            let xValue = button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
            let yValue = button.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
            
            // add constraints to list
            buttonCons = [buttonHeight, buttonWidth, xValue, yValue]
            
            // activate constraints
            NSLayoutConstraint.activate(buttonCons)
            
            // activate button and connect to currentLocation()
            button.addTarget(self, action: #selector(MapViewController.currentLocation(_:)), for: .touchUpInside)
            
        }
        
        // create button in view
        createButton()
        
        // configure locationManager
        locationManager.delegate = self
        
        // best accuracy
        // needs more work
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // only access location when user is on app
        locationManager.requestWhenInUseAuthorization()
        
        // go to location when view is loaded
        locationManager.startUpdatingLocation()
        
        
        
    }
    
}
