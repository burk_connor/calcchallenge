//
//  WebViewController.swift
//  WorldTrotterV4
//
//  Created by Connor Burk on 7/24/17.
//  Copyright © 2017 Connor Burk. All rights reserved.
//

import Foundation

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // website for the web view
        let url = URL(string: "https://www.bignerdranch.com/?gclid=CPXl5-y4odUCFUdWDQodCPILZQ")
        
        // check for website
        if let unwrappedURL = url {
            
            let request = URLRequest(url: unwrappedURL)
            let session = URLSession.shared
            
            let task = session.dataTask(with: request) { (data, response, error) in
                
                if error == nil {
                    
                    self.webView.loadRequest(request)
                    
                } else {
                    
                    print("Error: \(error)")
                    
                }
                
            }
            
            task.resume()
            
        }
        
    }
    
}
